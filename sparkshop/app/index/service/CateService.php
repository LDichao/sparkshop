<?php
// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai  <876337011@qq.com>
// +----------------------------------------------------------------------

namespace app\index\service;

use app\model\goods\Goods;
use app\model\goods\GoodsCate;
use think\facade\Route;

class CateService
{
    /**
     * 分类列表
     * @param int $cateId
     * @param int $level
     * @param int $limit
     * @param string $sortPrice
     * @return array|string
     */
    public function getCateList(int $cateId, int $level, int $limit, string $sortPrice)
    {
        // 该分类信息
        $cateModel = new GoodsCate();
        $cateInfo = $cateModel->findOne([
            'id' => $cateId,
            'status' => 1
        ], 'pid')['data'];

        // 从导航菜单进来，默认全部一级分类
        if (empty($cateInfo)) {
            $cateInfo['pid'] = 0;
            $defaultCate = $cateModel->findOne([
                'status' => 1,
                'pid' => 0
            ], 'id')['data'];

            if (empty($defaultCate)) {
                return build404(['msg' => '商品分类数据有误']);
            }

            $cateId = $defaultCate['id'];
        }

        // 统计分类信息
        $cateList = $cateModel->getAllList([
            'level' => $level,
            'pid' => $cateInfo['pid'],
            'status' => 1
        ], 'id,level,name', 'sort desc')['data'];

        $res = $this->buildCrumbsAndCate($level, $cateId, $cateModel)['data'];
        $crumbs = $res['crumbs'];
        $cateIds = $res['cateIds'];

        $crumbs = array_reverse($crumbs);
        // 关联的商品信息
        $goodsModel = new Goods();
        $where[] = ['cate_id', 'in', $cateIds];
        $where[] = ['is_del', '=', 2];

        $order = 'id desc';
        if (!empty($sortPrice)) {
            $order = 'price ' . $sortPrice;
        }

        $goodsList = $goodsModel->getPageList($limit, $where, 'id,name,slider_image,sales,collects,price', $order)['data']
            ->each(function ($item) {
                $item->slider_image = json_decode($item->slider_image, true)['0'];
            })->toArray();

        return dataReturn(0, 'success', [
            'cate_id' => $cateId,
            'cate_list' => $cateList,
            'crumbs' => $crumbs,
            'goods_list' => $goodsList,
            'level' => $level,
            'price_type' => $sortPrice,
            'getMoreUrl' => Route::buildUrl('/index/cate', [
                'id' => $cateId,
                'level' => $level,
                'limit' => $limit,
                'page' => $goodsList['current_page'] + 1,
                'p_s' => $sortPrice
            ]),
            'search' => ''
        ]);
    }

    /**
     * 搜索
     * @param string $search
     * @return array
     */
    public function searchGoods(string $search): array
    {
        // 关联的商品信息
        $goodsModel = new Goods();
        $where[] = ['name', 'like', '%' . $search . '%'];
        $where[] = ['is_del', '=', 2];

        $goodsList = $goodsModel->getPageList(20, $where, 'id,name,slider_image,sales,collects,price')['data']
            ->each(function ($item) {
                $item->slider_image = json_decode($item->slider_image, true)['0'];
            })->toArray();

        // 统计分类信息
        $cateModel = new GoodsCate();
        $cateList = $cateModel->getAllList([
            'level' => 1,
            'pid' => 0,
            'status' => 1
        ], 'id,level,name', 'sort desc')['data'];

        return dataReturn(0, 'success', [
            'cate_id' => 0,
            'cate_list' => $cateList,
            'crumbs' => [],
            'goods_list' => $goodsList,
            'level' => 0,
            'price_type' => 0,
            'getMoreUrl' => '',
            'search' => $search
        ]);
    }

    /**
     * 构建数据
     * @param int $level
     * @param int $cateId
     * @param GoodsCate $cateModel
     * @return array
     */
    private function buildCrumbsAndCate(int $level, int $cateId, GoodsCate $cateModel): array
    {
        $cateIds = [];
        $crumbs = []; // 面包屑
        if ($level == 3) {

            $crumbs[2] = $cateModel->findOne([
                'id' => $cateId
            ], 'id,level,pid,name')['data']->toArray();
            $cateIds[] = $cateId;

            $crumbs[1] = $cateModel->findOne([
                'id' => $crumbs[2]['pid']
            ], 'id,level,pid,name')['data']->toArray();

            $crumbs[0] = $cateModel->findOne([
                'id' => $crumbs[1]['pid']
            ], 'id,level,pid,name')['data']->toArray();
        } else if ($level == 2) {

            $crumbs[1] = $cateModel->findOne([
                'id' => $cateId
            ], 'id,level,pid,name')['data']->toArray();
            $cateIds[] = $cateId;

            $crumbs[0] = $cateModel->findOne([
                'id' => $crumbs[1]['pid']
            ], 'id,level,pid,name')['data']->toArray();
            $cateIds[] = $crumbs[1]['id'];
        } else {

            $crumbs[] = $cateModel->findOne([
                'id' => $cateId
            ], 'id,level,pid,name')['data'];
            $cateIds[] = $cateId;

            // 顶级的二级
            $cateIdMaps = $cateModel->getAllList([
                'pid' => $cateId
            ], 'id,pid')['data']->toArray();
            if (!empty($cateIdMaps)) {
                $pids = [];
                foreach ($cateIdMaps as $vo) {
                    $pids[] = $cateIds[] = $vo['id'];
                }

                // 三级数据
                if (!empty($cateIds)) {
                    $cateIdMaps = $cateModel->getAllList([
                        ['pid', 'in', $pids]
                    ], 'id,pid')['data']->toArray();

                    if (!empty($cateIdMaps)) {
                        foreach ($cateIdMaps as $vo) {
                            $cateIds[] = $vo['id'];
                        }
                    }
                }
            }
        }

        return dataReturn(0, 'success', [
            'cateIds' => $cateIds,
            'crumbs' => $crumbs
        ]);
    }
}